SHELL := /bin/bash

html:
	pdm run jb build book

pdf:
	pdm run jb build book --builder pdflatex

clean:
	rm -rf book/_build

format:
	mdformat README.md book --wrap 88
	black book

sync:
	pdm sync --clean
	pdm run python -m bash_kernel.install

lock:
	pdm lock
