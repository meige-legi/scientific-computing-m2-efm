---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Standard types and basic statements

## Function calls

There are [built-in functions](https://docs.python.org/3/library/functions.html) and the
developers can of course define other functions. To call a function:

```{code-cell} ipython3
print("hello")
```

Some functions return a result.

```{code-cell} ipython3
round(1.2)
```

It's common to store the result in a variable:

```{code-cell} ipython3
my_var = round(1.2)
```

which can then be used:

```{code-cell} ipython3
print(my_var)
```

## Few standard types

- Simple types (`int`, `float`, `bool`, `complex`)
- Standard type `str`
- Standard type `list`
- Standard type `tuple`

### `int` (integers)

```{code-cell} ipython3
a = 4
c = -10

# binary notation (base 2)
b = 0b010

# octal notation (base 8)
o = 0o011

# hexadecimal (base 16)
h = 0x1CF0

a = int("1")  # base 10
a = int("111", 2)  # base 2
a = int("70", 8)  # base 8
a = int("16", 16)  # base 16
```

```{note}
`int` in Python 3 are impressive! No limit! See
https://docs.python.org/3.1/whatsnew/3.0.html#integers
```

#### Arithmetic operations

```{code-cell} ipython3
print(10 + 3)
print(10 - 3)
print(10 * 3)
print(10 / 3)  # float division
print(10 // 3)  # integer division
print(10 % 3)
```

### `bool` (booleans)

```{code-cell} ipython3
b = bool("1")
b = False
b = True
```

#### Comparison operations (bool)

- `==` equal
- `!=` différent
- `<` inferior
- `<=` inferior or equal
- `>` superior
- `>=` superior or equal

#### Keyword `is`: check identity

```{code-cell} ipython3
a = None
print(a is None)
print(a is not None)
```

#### Keywords `and` and `or`

```{code-cell} ipython3

True and True
```

```{code-cell} ipython3
True and False
```

```{code-cell} ipython3
False and False
```

```{code-cell} ipython3
True or True
```

```{code-cell} ipython3
True or False
```

```{code-cell} ipython3
False or False
```

### `float` (real, double precision) and `complex`

```{code-cell} ipython3
# float
a = float("1")
a = 1.234
a = 1e2
a = -1e-2
a = 0.2
```

```{code-cell} ipython3
# complex (2 floats)
c = complex("1")
c = 1 + 2j
print(c, c.real, c.imag)
```

```{note}
Notation `var_name.attr_name` to access to an attribute of an object.
```

````{admonition} Warning about floating-point arithmetic and numerical errors!
---
class: warning
---
```python
b = 1e16
c = 1.2 + b
d = c - b
print(d)
```

gives `2.0`. It's a very general issue (not Python): see
https://en.wikipedia.org/wiki/Floating-point_arithmetic.

````

### Standard type `str`

```{code-cell} ipython3
s = "hello"
s = 'hello'

s = (
    "How is it possible to write a very very "
    "very long string with lines limited to 79 characters?"
)

s = """Strings on
more thand
one line.
"""
print(s)
```

```{admonition} Big difference between Python 2 and Python 3.
---
class: warning
---
In Python 3, `str` are unicode and there is another type `bytes`.
```

#### Methods of the type `str`

Objects of built-in types have methods associated with their type (object oriented
programming). The built-in function `dir` returns a list of name of the attributes. For
a string, these attributes are python system attributes (with double-underscores) and
several public methods:

```{code-cell} ipython3
# create a nicer "pretty print" function
from pprint import pprint
from functools import partial
pprint = partial(pprint, width=82, compact=True)
```

```{code-cell} ipython3

s = "abcdef"
pprint(dir(s))
```

Let's hide the internal variables starting and ending with `__` (for now you don't need
to understand the code used for that).

```{code-cell} ipython3
pprint([name_attr for name_attr in dir(s) if not name_attr.startswith("__")])
```

To access an attribute of an object (here, the method `str.startswith`), we use the dot:

```{code-cell} ipython3
s.startswith("a")
```

#### function `str.format`

```text
Docstring:
S.format(*args, **kwargs) -> str

Return a formatted version of S, using substitutions from args and kwargs.
The substitutions are identified by braces ('{' and '}').
```

```{code-cell} ipython3
a = 1.23456789
"a = {}".format(a)
```

```{code-cell} ipython3
"a = {:.4f}".format(a)
```

```{code-cell} ipython3
"a = {:8.4f}".format(a)
```

```{code-cell} ipython3
"a = {:.4e} (scientific notation)".format(a)
```

```{code-cell} ipython3
print("{}\t{}\t{}".format(1, 2, 3))
```

#### New in Python 3.6: format strings

```{code-cell} ipython3
a = 1.23456789
f"a = {a}"
```

```{code-cell} ipython3
f"a = {a:.4f}"
```

```{code-cell} ipython3
f"a = {a:8.4f}"
```

```{code-cell} ipython3
f"a = {a:.4e} (scientific notation)"
```

```{code-cell} ipython3
print(f"{1}\t{1+1}\t{2+1}")
```

#### Strings are immutable **["sequences"](https://docs.python.org/3/library/stdtypes.html)**.

- lookup

```{code-cell} ipython3
s = "abcdef"
print("a" in s)
print("hello" not in s)
```

- We can get an element of a string (**index starts from 0**):

```{code-cell} ipython3
print(s[0])
```

- since strings are immutable, they can not be modified inplace. If we try, we get an
  error:

```python
s[0] = 'b'
```

```text
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-5-55620f378bce> in <module>()
----> 1 s[0] = 'b'

TypeError: 'str' object does not support item assignment
```

- since strings are sequences, they can be "sliced" (we will soon study in details this
  powerful notation):

```{code-cell} ipython3
s[1:3]
```

- it is very simple to manipulate strings in many ways:

```{code-cell} ipython3
print((s.capitalize() + " " + s.upper() + "\n") * 4)
```

### Slicing

Very general, can be used on all sequences as `str`, `list`, etc... Not simple for
beginners but very powerfull (see
[here](http://stackoverflow.com/questions/509211/explain-pythons-slice-notation) and
[here](http://avilpage.com/2015/03/a-slice-of-python-intelligence-behind.html)).

Python indexes and slices for a six-element str. Indexes enumerate the elements, slices
enumerate the spaces between the elements.

```text
Index from rear:    -6  -5  -4  -3  -2  -1
Index from front:    0   1   2   3   4   5
                   +---+---+---+---+---+---+
                   | a | b | c | d | e | f |
                   +---+---+---+---+---+---+
Slice from front:  0   1   2   3   4   5   6
Slice from rear:  -6  -5  -4  -3  -2  -1   None
```

```{code-cell} ipython3
s = "abcdef"
# s[start:stop:step]
s[2:6:2]
```

```{code-cell} ipython3
# s[start:stop]
s[2:6]
```

```{code-cell} ipython3
# s[start:]
s[1:]
```

```{code-cell} ipython3
# s[:stop]
s[:2]
```

```{code-cell} ipython3
# step = -1: goes through the string in reverse order
s[::-1]
```

```{exercise-start}
---
label: exercise-str
---
```

Suppose we have a string representing a header line of the form:

```{code-cell} ipython3

s = " wind;temperature;;pressure "
```

1. Remove leading and ending blanks (with `str.strip`)
2. Extract the first field (with `str.find` and slicing)
3. Extract the last field (with `str.rfind` and slicing)
4. Check for empty field (with the `";;" in ...` pattern)
5. Remove empty field (with `str.replace`)

```{tip}
When you have an object of type `str` (here the variable `s`), 
do not use the methods with the code `str.strip(s)` but instead 
just access the method from your object, for example `s.strip()`.
```

For each task, do not hesitate to print the help of the method by writing `s.strip?` in
IPython.

```{exercise-end}
```

```{solution-start} exercise-str
---
class: dropdown
---
```

```{code-cell} ipython3
s = " wind;temperature;;pressure "
# remove leading blanks
s = s.strip()
f"--{s}--"
```

```{code-cell} ipython3
# extract the first field
idx = s.find(";")
field0 = s[0:idx]
field0
```

```{code-cell} ipython3
# extract the second field
idx1 = s.find(";", idx + 1)  # start the search after the first ";"
field1 = s[idx + 1 : idx1]
field1
```

```{code-cell} ipython3
# extract the last field
idx2 = s.rfind(";")
field2 = s[idx2 + 1 :]
field2
```

```{code-cell} ipython3
# check presence of ";;"
";;" in s
```

```{code-cell} ipython3
# remove empty field
s_no_empty = s.replace(";;", ";")
s_no_empty
```

```{solution-end}
```

### standard type `list`

A list is a **mutable sequence of (possibly inhomogeneous) elements**.

```{code-cell} ipython3
type([0, "a"])
```

```{code-cell} ipython3
# create an empty list
l = []
# fill the list (with the function append)
l.append("2")
# fill the list (with the function extend)
l.extend([6, 3.0])
print(l)
```

```{code-cell} ipython3
# concatenate lists with the operator +
print(l + ["hello", 3])
```

```{code-cell} ipython3
# get values
print(l[0], l[2], l[-2])
# slicing
print(l[0:2])
```

### standard type `tuple`

A tuple is a **immutable sequence of (possibly inhomogeneous) elements**.

```{note}
When you need a sequence that won't be modified, tuple is usually more
efficient than list.
```

```{code-cell} ipython3
t = 0, "a", 1.2
t1 = (5, "hello")
t2 = tuple([1.1, 2])
type(t)
```

```{code-cell} ipython3
t[1]  # indexing
```

```{code-cell} ipython3
t[1:]  # slicing
```

```{code-cell} ipython3
a, b = t1  # tuple assigment
print(b)
```

## Mutable and immutable objects

### Immutable objects

The objects of type `str`, `int`, `float`, `bool` are immutable. They can not be
modified. Of course, a name that points towards an integer can point towards a different
integer.

```{code-cell} ipython3
i = 1
i = i + 2  # (or i += 2)
print(i)
i = 10
print(i)
```

Here, the objects `1` and `3` have not been modified.

### Mutable objects

An object of type `list` is mutable:

```{code-cell} ipython3
l = [0, 5]
print(l)
l.append("hello")
print(l)
```

Here, the object list tagged by the name `l` has been modified inplace.

## References and `del` keyword ("delete")

`del` removes a reference. If an object in not bound to any names, Python can delete it
from its internal memory.

```{code-cell} ipython3
l = ["a", "b"]
del l[1]
print(l)
```

## More on slicing

Very general, can be used on all sequences as `str`, `list`, etc... Not simple for
beginners but very powerfull (see
[here](http://stackoverflow.com/questions/509211/explain-pythons-slice-notation) and
[here](http://avilpage.com/2015/03/a-slice-of-python-intelligence-behind.html)).

Python indexes and slices for a six-element str. Indexes enumerate the elements, slices
enumerate the spaces between the elements.

```text
Index from rear:    -6  -5  -4  -3  -2  -1
Index from front:    0   1   2   3   4   5
                   +---+---+---+---+---+---+
                   | a | b | c | d | e | f |
                   +---+---+---+---+---+---+
Slice from front:  0   1   2   3   4   5   6
Slice from rear:  -6  -5  -4  -3  -2  -1   0
```

```{code-cell} ipython3
s = "abcdef"
# s[start:stop:step]
s[2:6:2]
```

### Assignment to mutable object

```{code-cell} ipython3
l = [0, 1, 2, 3, 4, 5]
l1 = l  # assignment to a new name l1 (no copy of the object).
# the names l and l1 points towards the same object.
l1.append("a")
print(l1)
print(l)
```

### Shallow copy

```{code-cell} ipython3
l = [0, 1, 2, 3, 4, 5]
l1 = l[:]  # shallow copy of l (same as `l1 = l.copy()`)
l1.append("a")
print(l1)
print(l)
```

Other examples of slices for a six-element list. Indexes enumerate the elements, slices
enumerate the spaces between the elements.

```{code-cell} ipython3
a = [0, 1, 2, 3, 4, 5]
all(
    [
        len(a) == 6,
        a[1:] == [1, 2, 3, 4, 5],
        a[:5] == [0, 1, 2, 3, 4],
        a[0] == 0,
        a[:-2] == [0, 1, 2, 3],
        a[5] == 5,
        a[1:2] == [1],
        a[-1] == 5,
        a[1:-1] == [1, 2, 3, 4],
        a[-2] == 4,
    ]
)
```

```{exercise-start}
---
label: exercise-parse-header
---
```

Suppose we have the string containing header line.

```{code-cell} ipython3
s = "wind;temperature;pressure"
```

- Extract the list of items (i.e. "wind", "temperature", "pressure"; see `str.split`).
- Add "Snow level" to the list of items (see `list.append`)
- Capitalize all elements of the list (with `str.capitalize` and iterating on the list)
- Build a new header such that items are capitalized (with `str.join`)

```{exercise-end}
```

```{solution-start} exercise-parse-header
---
class: dropdown
---
```

```{code-cell} ipython3
list_items = s.split(";")
list_items
```

```{code-cell} ipython3
list_items.append("Snow level")
list_items[0] = list_items[0].capitalize()
list_items[1] = list_items[1].capitalize()
list_items[2] = list_items[2].capitalize()
list_items
```

```{code-cell} ipython3
";".join(list_items)
```

```{solution-end}
```

## The function `range`

The function returns a range object:

```{code-cell} ipython3
# start, stop, step
range(1, 8, 2)
```

We can make a list with the range object:

```{code-cell} ipython3
# start, stop, step
list(range(1, 8, 2))
```

```{code-cell} ipython3
# start, stop (step=1)
list(range(2, 8))
```

```{code-cell} ipython3
# stop argument (start=0, step=1)
list(range(8))
```

```{exercise}
---
label: exercise-odd-numbers
---
Build a list of odd numbers in decreasing order.

```

## Conditions: `if`, `elif`, `else`

```python
if expression:
   statement(s)
else:
   statement(s)
```

The statement contains the block of code that executes if the conditional expression in
the if statement resolves to 1 or a `True` value.

```{code-cell} ipython3
a = 0
if a == 0:
    print("a is equal to 0.")
```

```{code-cell} ipython3
a = 1
if a < 0:
    print("a is negative.")
elif a == 0:
    print("a is equal to 0.")
elif a > 0:
    print("a is positive.")
else:
    print("I don't know.")
```

## Loops

### Loops with the keyword `while`

```{code-cell} ipython3
i = 0
while i < 4:
    i += 1
print("i =", i)
```

```{code-cell} ipython3
i = 0
while i < 4:
    i += 1
    print("i =", i)
```

```{exercise}
---
label: exercise-average
---
- Edit a script with Spyder that calculates the average of a set of numbers. For example `numbers = [67, 12, 2, 9, 23, 5]`

  - using the functions `sum` and `len`
  - manually (without `sum`), using the keyword `while`
  - check that the 2 methods give the same results with

  `assert avg0 == avg1`

- Run the script

  - in Spyder,
  - in a IPython session opened from another terminal,
  - with the command `python`.

```

```{solution-start} exercise-average
---
class: dropdown
---
```

```{code-cell} ipython3
numbers = [67, 12, 2, 9, 23, 5]

avg0 = sum(numbers) / len(numbers)

tmp = 0
i = 0
while i < len(numbers):
    tmp += numbers[i]
    i = i + 1
avg1 = tmp/ len(numbers)

assert avg0 == avg1
```

```{solution-end}
```

### Loops with the keyword `for`

```{code-cell} ipython3
values = range(5)
for value in values:
    print(value, end=", ")
```

```{code-cell} ipython3
# the built-in function enumerate is very useful
for index, value in enumerate(["a", "b", "c"]):
    print(f"({index}, {value})")
```

### Loops: keywords `continue` and `break`

- `continue`: passes the block in the loop and continues the loop.

```{code-cell} ipython3

for x in range(1, 8):
    if x == 5:
        continue
    print(x, end=", ")
```

- `break`: stop the loop.

```{code-cell} ipython3

for x in range(1, 11):
    if x == 5:
        break
    print(x, end=", ")
```

```{exercise}
---
label: exercise-for-enumerate
---
- Extend your script with another method (using a `for` loop) to compute the average.

- In IPython, try to understand how the function `enumerate` works. Use it in your script.

```

```{solution-start} exercise-for-enumerate
---
class: dropdown
---
```

```{code-cell} ipython3
l = [67, 12, 2, 9, 23, 5]
avg2 = 0
for i, e in enumerate(l):
    avg2 += e
avg2 /= i + 1
assert avg2 == sum(l)/len(l)
```

```{solution-end}
```

```{exercise-start}
---
label: ex1
---
```

We build a list:

```{code-cell} ipython3
from random import randint, shuffle

n = 20
i = randint(0, n - 1)
print("integer remove from the list:", i)
numbers = list(range(n))
numbers.remove(i)
shuffle(numbers)
print(f"shuffled list:\n  {numbers}")
```

One element has been removed:

- Find this element (given that you can change the ordering of `l`).
- Find this element (given that you cannot change the ordering of `l`).

```{exercise-end}
```

```{solution-start} ex1
---
class: dropdown
---
```

```{code-cell} ipython3
# we can change ordering, let's sort
print(numbers)
l_sorted = sorted(numbers)
print(l_sorted)
found = None
for idx, elem in enumerate(l_sorted):
    if elem != idx:
        found = idx
        break
if found is None:
    found = len(numbers)
print("missing ", idx)
```

```{code-cell} ipython3
# we cannot sort -> higher complexity
for elem in range(len(numbers) + 1):
    if elem not in numbers:
        break
print("missing ", elem)
```

```{code-cell} ipython3
# another solution
actual_sum = sum(numbers)
len_numbers = len(numbers)
original_sum = (len_numbers + 1) * (len_numbers) // 2
print("missing ", original_sum - actual_sum)
```

```{solution-end}
```

## Exceptions and `try`, `except` syntax

Exceptions and errors are common in all codes. There is a good system to handle them in
Python. Let's first see what gives such buggy code

```python
letters = "abc"
i = 3
print(letters[i])
```

When these lines are executed, Python stops its execution and print a traceback:

```text
---------------------------------------------------------------------------
IndexError                                Traceback (most recent call last)
<ipython-input-30-8df9cec1a0ec> in <module>()
      1 letters = "abc"
      2 i = 1
----> 3 print(letters[i])

IndexError: list index out of range
```

Handling exception:

```{code-cell} ipython3
letters = "abc"
i = 3
try:
    print(letters[i])
except IndexError as e:
    print(f"An IndexError has been raised and caught (message: '{e}')")
```

````{warning}
Never use

```python
except:
```

It means "except all errors and exceptions". A user Control-C is an exception
(`KeyboardInterrupt`) so it would be caught and have no effect.
````

### Full syntax

```python
try:
    ...
except <exception1> as e1:
    ...
except <exception2> as e2:
    ...
else:
    ...
finally:
    ...
```

[Non exhaustive error list](https://docs.python.org/3/library/exceptions.html):

- ArithmeticError
- ZeroDivisionError
- IndexError
- KeyError
- AttributeError
- IOError
- ImportError
- NameError
- SyntaxError
- TypeError

```{exercise-start}
---
label: exercice-exceptions
---
```

For each line of this string, append a variable of correct type in a list (i.e. "hello"
should stay hello, 2 and 1.5 should become floats). Do it by catching errors.

```{code-cell} ipython3
str_variables = "hello 1.5 2"

the_list_you_should_get = ["hello", 1.5, 2.0]
```

**Hints:**

- float("a") raise ValueError,
- the str `str_variable` should be split.

```{exercise-end}
```

```{solution-start} exercice-exceptions
---
class: dropdown
---
```

```{code-cell} ipython3
split_list = []
for value in str_variables.split():
    try:
        value = float(value)
    except ValueError:
        print(value, "is not a number")
    split_list.append(value)


print(split_list)
```

```{solution-end}
```
