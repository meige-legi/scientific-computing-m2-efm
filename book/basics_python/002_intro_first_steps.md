---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# First steps. Different ways to use Python.

## Python: a language and some interpreters

Python is a programming language.

The most common way to execute Python code is to **interpret** it. Through misuse of
language, one says that Python is an **interpreted** language (like Bash, Matlab, and in
contrast with Fortran, C or C++). To interpret code, we need an interpreter, i.e. a
program parsing Python code and running computer instructions.

Open a terminal. There are usually different python commands: `python`, `python2` and
`python3`.

```{admonition} Python 2 / Python 3

2 languages, 2 interpreters, 2 commands

```

## How to get Python

- On Linux, there are the system python binaries (for example `/usr/bin/python3`).

- On Windows, Mac (and Linux), you can install and use

  - https://www.python.org/downloads/

  - [Anaconda](https://www.anaconda.com/products/distribution),
    [Miniconda](https://docs.conda.io/en/latest/miniconda.html) or (even better)
    [Miniforge](https://github.com/conda-forge/miniforge)

## Download the repository of this training

````{exercise}
---
label: exercise-clone
---
Download the repository of this training (you need one of the programs `git` or
`mercurial`). On Unix machines, use the commands:

```bash
hg clone https://gricad-gitlab.univ-grenoble-alpes.fr/meige-legi/scientific-computing-m2-efm.git
cd scientific-computing-m2-efm/book/basics_python
ls
```

````

## Execute a script with the command `python3`

````{exercise}
---
label: exercise-exec-script
---
Run a script with `python3` (`pyfiles/helloworld.py` is *just* a text file):

```bash
cat pyfiles/helloworld.py
python3 pyfiles/helloworld.py
```

```{note}
`cat` is a Unix command that prints the content of a text file.
```

````

## Work interactively with [ipython](https://ipython.org/)

The command `ipython3` launches the program [IPython](https://ipython.org/), which is
used for interactive python.

```{exercise-start}
---
label: ex-ipython
---
```

- In IPython, you can execute a first interactive instruction:

```{code-cell} ipython3
2 + 2
```

```{code-cell} ipython3
3 / 4
```

- Run the script from ipython.

```{code-cell} ipython3
run pyfiles/helloworld.py
```

```{code-cell} ipython3
# the variable name is defined in ../pyfiles/helloworld.py
print(name)
```

- Help on an object can be displayed with the question mark (try it):

```ipython
name?
```

- Try to type "name." and to press on tab... The tab key is used to tell you how what
  you typed can be completed.

- Try to use the top and bottom arrows...

```{exercise-end}
```

## Python in an IDE ([Spyder](https://pythonhosted.org/spyder/))

Launch the application Spyder, a Python IDE (Integrated development environment).

- a good code editor with:
  - syntax coloring,
  - code analysis powered by pyflakes and pylint,
  - introspection capabilities such as code completion.
- Ipython console
- variable inspector
- ...

```{important}
It is very important to use a good editor to code in Python (for example Spyder,
emacs or vi (**with a good setup!**), or
[Visual Studio Code](https://code.visualstudio.com/docs/languages/python)).
```

## Python in the browser ([JupyterLab](https://jupyter.readthedocs.io))

The presentations of this python training are made with Jupyter (demonstration).

This is a very powerful tool to present results (see these
[examples](http://nbviewer.jupyter.org/)).

```bash
jupyter-lab
```
