---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Data structures

See <https://docs.python.org/3/tutorial/datastructures.html>

4 built-in containers: list, tuple, set and dict...

For more containers: see
[collections](https://docs.python.org/3/library/collections.html)...

## list: mutable sequence

Lists are mutable ordered tables of inhomogeneous objects. They can be viewed as an
array of references (nearly pointers) to objects.

```{code-cell} ipython3
# 2 equivalent ways to define an empty list
l0 = []
l1 = list()
assert l0 == l1

# not empty lists
l2 = ["a", 2]
l3 = list(range(3))
print(l2, l3, l2 + l3)
print(3 * l2)
```

The [`itertools`](https://docs.python.org/3/library/itertools.html) module provide other
ways of iterating over lists or set of lists (e.g. cartesian product, permutation,
filter, ... ).

The builtin function `dir` returns a list of name of the attributes. For a list, these
attributes are python system attributes (with double-underscores) and 11 public methods:

```{code-cell} ipython3
# create a nicer "pretty print" function
from pprint import pprint
from functools import partial
pprint = partial(pprint, width=82, compact=True)
```

```{code-cell} ipython3
pprint(dir(l3))
```

```{code-cell} ipython3
l3.append(10)
print(l3)
l3.reverse()
print(l3)
```

```{code-cell} ipython3
# Built-in functions applied on lists
# return lower value
print(min(l3))
# return higher value
print(max(l3))
# return sorted list
print(sorted([5, 2, 10, 0]))
```

```{code-cell} ipython3
# "pasting" two lists can be done using zip
l1 = [1, 2, 3]
s = "abc"
print(list(zip(l1, s)))
print(list(zip("abc", "defg")))
```

### `list`: list comprehension

They are iterable so they are often used to make loops. We have already seen how to use
the keyword `for`. For example to build a new list (side note: `x**2` computes `x^2`):

```{code-cell} ipython3
l0 = [1, 4, 10]
l1 = []
for number in l0:
    l1.append(number**2)

print(l1)
```

There is a more readable (and slightly more efficient) method to do such things, the
"list comprehension":

```{code-cell} ipython3
l1 = [number**2 for number in l0]
print(l1)
```

```{code-cell} ipython3
# list comprehension with a condition
[s for s in ["a", "bbb", "e"] if len(s) == 1]
```

```{code-cell} ipython3
# lists comprehensions can be cascaded
[(x, y) for x in [1, 2] for y in ["a", "b"]]
```

```{exercise} advanced
---
label: exercise-extract-patterns
---
- Write a function `extract_patterns(text, n=3)` extracting the list of patterns
  of size `n=3` from a long string (e.g. if `text = "basically"`, patterns would
  be the list `['bas', 'asi', 'sic', ..., 'lly']`). Use list comprehension, range,
  slicing. Use a sliding window.

- You can apply your function to a long "ipsum lorem" string (ask to your favorite
  web search engine).

```

```{solution-start} exercise-extract-patterns
---
class: dropdown
---
```

```{code-cell} ipython3
text = "basically"


def extract_patterns(text, n=3):
    pat = [text[i : i + n] for i in range(len(text) - n + 1)]
    return pat


print("patterns=", extract_patterns(text))
print("patterns=", extract_patterns(text, n=5))
```

```{solution-end}
```

## `tuple`: immutable sequence

Tuples are very similar to lists but they are immutable (they can not be modified).

```{code-cell} ipython3
---
jupyter:
  outputs_hidden: true
---
# 2 equivalent notations to define an empty tuple (not very useful...)
t0 = ()
t1 = tuple()
assert t0 == t1

# not empty tuple
t2 = (1, 2, "a")  # with the parenthesis
t2 = 1, 2, "a"  # it also works without parenthesis
t3 = tuple(l3)  # from a list
```

```{code-cell} ipython3
# tuples only have 2 public methods (with a list comprehension)
[name for name in dir(t3) if not name.startswith("__")]
```

```{code-cell} ipython3
# assigment of multiple variables in 1 line
a, b = 1, 2
print(a, b)
# exchange of values
b, a = a, b
print(a, b)
```

Tuples are used *a lot* with the keyword `return` in functions:

```{code-cell} ipython3
def myfunc():
    return 1, 2, 3


t = myfunc()
print(type(t), t)
# Directly unpacking the tuple
a, b, c = myfunc()
print(a, b, c)
```

## `set`: a hashtable

Unordered collections of unique elements (a hashtable). Sets are mutable. The elements
of a set must be [hashable](https://docs.python.org/3/glossary.html#term-hashable).

```{code-cell} ipython3
---
jupyter:
  outputs_hidden: true
---
s0 = set()
```

```{code-cell} ipython3
{1, 1, 1, 3}
```

```{code-cell} ipython3
set([1, 1, 1, 3])
```

```{code-cell} ipython3
s1 = {1, 2}
s2 = {2, 3}
print(s1.intersection(s2))
print(s1.union(s2))
```

### `set`: lookup

Hashtable lookup (for example `1 in s1`) is algorithmically efficient (complexity O(1)),
i.e. theoretically faster than a look up in a list or a tuple (complexity O(size
iterable)).

```{code-cell} ipython3
print(1 in s1, 1 in s2)
```

### What is a hashtable?

https://en.wikipedia.org/wiki/Hash_table

```{code-cell} ipython3
from random import shuffle, randint

n = 20
i = randint(0, n - 1)
print("integer remove from the list:", i)
l = list(range(n))
l.remove(i)
shuffle(l)
print("shuffled list: ", l)
```

### Back to the "find the removed element" problem

- Could the problem be solved using set ?
- What is the complexity of this solution ?

+++

A possible solution :

```{code-cell} ipython3
full_set = set(range(n))
changed_set = set(l)
ns = full_set - changed_set
ns.pop()
```

#### Complexity

- line 1: n insertions --> O(n)
- line 2 : n insertions --> O(n)
- line 3: one traversal O(n), with one lookup at each time (O(1) -> O(n)

=> Complexity of the whole algorithm : O(n)

Complexity of the "sum" solution : one traversal for the computation of the sum O(n)
with sum at each step O(1) => O(n).

## `dict`: unordered set of key: value pairs

The dictionary (`dict`) is a very important data structure in Python. All namespaces are
(nearly) dictionaries and "Namespaces are one honking great idea -- let's do more of
those!" (The zen of Python).

A dict is a hashtable (a set) + associated values.

```{code-cell} ipython3
d = {}
d["b"] = 2
d["a"] = 1
print(d)
```

```{code-cell} ipython3
d = {"a": 1, "b": 2, 0: False, 1: True}
print(d)
```

### Tip: parallel between `dict` and `list`

You can first think about `dict` as a super `list` which can be indexed with other
objects than integers (and in particular with `str`).

```{code-cell} ipython3
l = ["value0", "value1"]
l.append("value2")
print(l)
```

```{code-cell} ipython3
l[1]
```

```{code-cell} ipython3
d = {"key0": "value0", "key1": "value1"}
d["key2"] = "value2"
print(d)
```

```{code-cell} ipython3
d["key1"]
```

But warning, `dict` are not ordered (since they are based on a hashtable)!

### `dict`: public methods

```{code-cell} ipython3
# dict have 11 public methods (with a list comprehension)
pprint([name for name in dir(d) if not name.startswith("__")])
```

### `dict`: different ways to loops over a dictionary

```{code-cell} ipython3
# loop with items
for key, value in d.items():
    if isinstance(key, str):
        print(key, value)
```

```{code-cell} ipython3
# loop with values
for value in d.values():
    print(value)
```

```{code-cell} ipython3
# loop with keys
for key in d.keys():
    print(key)
```

```{code-cell} ipython3
# dict comprehension (here for the "inversion" of the dictionary)
print(d)
d1 = {v: k for k, v in d.items()}
```

```{exercise-start}
---
label: ex-number-occurrences
---
```

Write a function that returns a dictionary containing the number of occurrences of
letters in a text.

```{code-cell} ipython3
text = "abbbcc"
```

```{exercise-end}
```

```{solution-start} ex-number-occurrences
---
class: dropdown
---
```

```{code-cell} ipython3
def count_elem(sequence):
    d = {}

    for letter in sequence:
        if letter not in d:
            d[letter] = 1
        else:
            d[letter] += 1
    return d


print(f"{text = }, {count_elem(text) = }")
```

```{solution-end}
```
