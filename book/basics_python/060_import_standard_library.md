---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Modules, `import` statement and the standard library

## Motivations: import code from other files

Useful for:

- reusing code
- organizing code
- distributing code (not detailed here)

```{admonition} Definitions: modules and packages
- A module is a python file that can be imported.
- A package is a directory containing module(s).
```

## Multi-file program and imports

### Simple case of a directory with 2 files

Let's study this very simple "package" (i.e. a directory) containing 2 files:

```{code-cell} ipython3
import os

os.listdir("pyfiles/example0")
```

The file `util.py` contains:

```{eval-rst}
.. literalinclude:: pyfiles/example0/util.py
```

Let's run it:

```{code-cell} ipython3
run pyfiles/example0/util.py
```

The file `prog.py` contains:

```{eval-rst}
.. literalinclude:: pyfiles/example0/prog.py
```

Let's run it:

```{code-cell} ipython3
run pyfiles/example0/prog.py
```

```{note}
Files imported more than once are executed only once per process.
```

### More advanced notions

In the directory `example1`, we also have 2 files:

```{code-cell} ipython3
os.listdir("pyfiles/example1")
```

The files are just slightly modified versions of the files in `pyfiles/example0`. The
goal is to try to understand:

- the very common idiom `if __name__ == "__main__": ...`.
- where the Python interpreters looks for module and the attribute `sys.path`.

`pyfiles/example1/util.py` contains:

```{eval-rst}
.. literalinclude:: pyfiles/example1/util.py
```

Let's study what it gives when one executes it directly:

```{code-cell} ipython3
run pyfiles/example1/util.py
```

And `pyfiles/example1/prog.py` contains:

```{eval-rst}
.. literalinclude:: pyfiles/example1/prog.py
```

Let's run it:

```{code-cell} ipython3
run pyfiles/example1/prog.py
```

```{admonition} sys.path

From <https://docs.python.org/3/library/sys.html>:

> A list of strings that specifies the search path for modules. Initialized from
> the environment variable PYTHONPATH, plus an installation-dependent default.

> As initialized upon program startup, the first item of this list, path\[0\], is
> the directory containing the script that was used to invoke the Python
> interpreter.

```

## Warning about syntax `from ... import *`

There is another import syntax with a start:

```{code-cell} ipython3
from matplotlib.pylab import *
```

It imports in the global namespace all names of the namespace `matplotlib.pylab`. It can
be useful in some situations but should be avoid in many cases. With this syntax, you
don't know from where come the names and automatic code analysis becomes much more
difficult.

## Standard structure of a Python module

```{code-cell} ipython3
"""A program...

Documentation of the module.

"""

# import functions, modules and/or classes

from math import sqrt

# definition of functions and/or classes


def mysum(variables):
    """sum all the variables of the function and return it.
    No type check
    :param variables: (iterable) an iterable over elements
                      that can be summed up
    :return: the sum of the variables
    """
    result = 0
    for var in variables:
        result += var
    return result


# main part of the program (protected)

if __name__ == "__main__":
    l = [1, 2, 3, 4]
    print("the square of mysum(l) is", sqrt(mysum(l)))
```

## The standard library + presentation of few very common packages

The [Python standard library](https://docs.python.org/3/library/) (see also
[this tuto](https://docs.python.org/3/tutorial/stdlib.html)) is a quite impressive set
of packages useful for many many things. These packages are included nativelly in
Python. They are very stable (difficult to find bugs). Here is a small list:

- math - Mathematical functions
- sys - System-specific parameters and functions (a lot about the Python system)
- copy - Shallow and deep copy operations
- os - Miscellaneous operating system interfaces
- glob - Unix style pathname pattern expansion (like `ls`)
- shutil - High-level file operations
- pdb - activate the python debugger
- subprocess
- datetime
- pickle - Python object serialization
- re - Regular expressions
- argparse - Parser for command-line options, arguments and sub-commands
- unittest - Unit testing framework
- logging - Event logging system
- platform - Access to underlying platform’s identifying data
- threading - Higher-level threading interface
- multiprocessing - Process-based “threading” interface

### math - Mathematical functions

For example to use $\pi$ in an environment where Numpy might not be installed:

```{code-cell} ipython3
import math

print(type(math))
```

```{code-cell} ipython3
from math import cos

print("pi is approximately equal to     ", math.pi)
print("cos(pi) is approximately equal to", cos(math.pi))
```

### sys - System-specific parameters and functions (a lot about the Python system)

If you want to know where Python looks for module during the import statements, you can
do

```{code-cell} ipython3
import sys

pprint(sys.path)
```

### os: Miscellaneous operating system interfaces

os is a very important module.

```{code-cell} ipython3
import os

os.getcwd()
```

There is in particular the `os.path` module, which you use each time you work with paths
towards files and directories. It can be used to build paths in the most robust manner:

```{code-cell} ipython3
# Building a path to a file to read...
directory_path = "./files/"
file_name = "file_to_read.txt"
# String concatenation works but is not very robust
full_path = directory_path + file_name
print(full_path)
# Better to do
full_path = os.path.join(directory_path, file_name)
print(full_path)
```

For example, we can create the string for a new path in a cross-platform way like this

```{code-cell} ipython3
# Method to get cross-platform home directory ($HOME)
home_dir = os.path.expanduser("~")
os.path.join(home_dir, "opt", "miniconda3", "lib/python3.6")
```

To make a new directory if it does not exist:

```{code-cell} ipython3
path_tmp = "/tmp/tmp_dir_example_python"

import shutil
shutil.rmtree(path_tmp, ignore_errors=True)

if not os.path.exists(path_tmp):
    os.mkdir(path_tmp)
pprint(os.listdir("pyfiles/"))
```

To scan the content of a directory:

```{code-cell} ipython3
def list_dir_files():
    for base, path_dir, path_files in os.walk("pyfiles"):
        if base.startswith("__"):
            continue
        print(
            (
                f"In the directory {base}:\n"
                f"\tdirectories: {path_dir}\n\tfiles {path_files}."
            )
        )


list_dir_files()
print(os.path.exists(path_tmp))
os.rmdir(path_tmp)
print(os.path.exists(path_tmp))
list_dir_files()
```

Other handy functions of `os.path`:

- `os.path.basename`: returns the basename of a path (last member of a path)
- `os.path.isfile`: returns True if the path points to a file
- ...

See https://docs.python.org/3.7/library/os.path.html

### glob - Unix style pathname pattern expansion

The equivalent of the Unix "ls" is in the `glob` module:

```{code-cell} ipython3
from glob import glob

l = glob("*")
print("list unsorted:", l)
print("list sorted:  ", sorted(l))
```

### pathlib: Object-oriented filesystem paths

A modern (Python 3) and nicer method to manipulate file paths.

```{code-cell} ipython3
from pathlib import Path
```

```{code-cell} ipython3
path_tmp = Path("/tmp/tmp_dir_example_python")
print(path_tmp.exists())
path_tmp.mkdir(exist_ok=True)

for i in range(4):
    (path_tmp / f"tmp{i}.xml").touch()
    (path_tmp / f"tmp{i}.txt").touch()

pprint(sorted(path_tmp.glob("*.txt")))
```

```{note}
Code using `libpath.Path` is often nicer than the equivalent using `os.path`
and `glob`.
```

### shutil - High-level file operations

Copy of files and directories can be done with shutil, in particular with
shutil.copytree.

### pdb: useful to debug code

On a script:

1. import pdb
2. write pdb.set_trace() to set up a breakpoint
3. run the script

At execution time, the script will stop at the first line containing pdb.set_trace() and
gives the user access to the interpreter.

Remarks:

- even nicer: `ipdb` (but not part of the standard library).

- even nicer: `breakpoint()` built-in function in Python 3.7.

### subprocess

subprocess is very important since it is the simple way to launch other programs and
bash commands from Python. For example, in order to run bash (and not sh) commands, you
can do

```{code-cell} ipython3
import subprocess


def call_bash(commands):
    return subprocess.call(["/bin/bash", "-c", commands])


ret = call_bash(
    """
echo Hello; ls /tmp
"""
)
if ret == 0:
    print("command succeed")
else:
    print(f"command failed with return code {ret}")
```

### argparse - Parser for command-line options, arguments and sub-commands

argparse is the right tool to develop a command line script with options and help.
Example from the tutorial at https://docs.python.org/3/howto/argparse.html :

```python3
# File prog.py
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("echo", help="echo the string you use here")
args = parser.parse_args()
print(args.echo)
```

#### Usage :

```bash
$ python3 prog.py
usage: prog.py [-h] echo
prog.py: error: the following arguments are required: echo
$ python3 prog.py --help
usage: prog.py [-h] echo

positional arguments:
  echo        echo the string you use here

optional arguments:
  -h, --help  show this help message and exit
$ python3 prog.py foo
foo
```

### logging - Event logging system

logging allows the programmer to print (or not) different levels of messages.

```{code-cell} ipython3
import logging

log_level = logging.INFO  # to get information messages
# log_level = logging.WARNING  # no information messages
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=log_level
)

thing = "beer"
logging.info('Would you like to have a "%s"?', thing)
```
