---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Read / write files

There are a lot of specialized tools to open specialized files (images, xml, csv, hdf5,
netcdf, etc.). Here we focus on the low-level general method to open files.

## [`open` built-in function](https://docs.python.org/3/library/functions.html#open) and file object

```{code-cell} ipython3
file = open("pyfiles/helloworld.py")
txt = file.read()
file.close()
print(txt)
```

But what if something weird happens when the file is open (e.g. a division by O) ?

-> Exception is raised that could be catch and run some code that is not aware of the
opening file.

-> The file remains open

### Context, `with` keyword

For such objects that need to be closed, it is a good practice to use the keyword `with`
(THIS IS MUCH BETTER than using the close function, USE `with`!). Like this, we are sure
that the file will be closed even if there is an error:

```{code-cell} ipython3
with open("pyfiles/helloworld.py") as file:
    txt = file.read()
print(txt)
```

### Loop over lines

```{code-cell} ipython3
with open("pyfiles/helloworld.py") as file:
    for line in file:
        print(f"line ?: " + line.strip())
```

And now using `enumerate` to get the index of the line:

```{code-cell} ipython3
with open("pyfiles/helloworld.py") as file:
    for i, line in enumerate(file):
        print(f"line {i:2d}: {line.strip()}")
```

### Options of the built-in function open (read, write, append)

```{code-cell} ipython3
# write data in a file

with open("/tmp/zoo.txt", "w") as file_zoo:
    file_zoo.write("sam;cat;2\n")
    file_zoo.write("liloo;lion;2\n")

with open("/tmp/zoo.txt", "a") as file_zoo:
    file_zoo.write("peter;panda;5\n")

with open("/tmp/zoo.txt") as file_zoo:
    print(file_zoo.read())
```

```{code-cell} ipython3
with open("/tmp/zoo.txt", "r") as file_zoo:
    print(file_zoo.readline())
    print(file_zoo.read())
```

Remark: difference `write` and `print`.

### Options of the built-in function open (binary file)

Until now, we have only written text files. It can of course be much more efficient to
use binary format.

```{code-cell} ipython3
with open("/tmp/test", "wb") as file:
    file.write(b"a")
```

Remarks:

- In practice, saving data in binary file is most of the time a bad idea. There are much
  better solutions to do this (see for example h5py and h5netcdf).

- There are Python libraries to read and process many types for files (csv, xml, json,
  images, tabulars, etc.).
