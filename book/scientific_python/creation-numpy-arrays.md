---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Creation of Numpy arrays

Code using `numpy` usually starts with the import statement

```{code-cell} ipython3
import numpy as np
```

NumPy provides the type `np.ndarray`. Such array are multidimensionnal sequences of
homogeneous elements. They can be created for example with the commands:

```{code-cell} ipython3
# from a list
l = [10.0, 12.5, 15.0, 17.5, 20.0]
np.array(l)
```

```{code-cell} ipython3
# fast but the values can be anything
np.empty(4)
```

```{code-cell} ipython3
# slower than np.empty but the values are all 0.
np.zeros([2, 6])
```

```{code-cell} ipython3
# multidimensional array
a = np.ones([2, 3, 4])
print(a.shape, a.size, a.dtype)
a
```

```{code-cell} ipython3
# like range but produce 1D numpy array
np.arange(4)
```

```{code-cell} ipython3
# np.arange can produce arrays of floats
np.arange(4.0)
```

```{code-cell} ipython3
# another convenient function to generate 1D arrays
np.linspace(10, 20, 5)
```

A NumPy array can be easily converted to a Python list.

```{code-cell} ipython3
a = np.linspace(10, 20, 5)
list(a)
```

```{code-cell} ipython3
# Or even better
a.tolist()
```
