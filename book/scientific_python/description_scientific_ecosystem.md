---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Python scientific ecosystem

There are a lot of very good Python packages for sciences. The fundamental packages are
in particular:

- [numpy](http://www.numpy.org/): numerical computing with powerful numerical arrays
  objects, and routines to manipulate them.
- [scipy](http://www.scipy.org/): high-level numerical routines. Optimization,
  regression, interpolation, etc.
- [matplotlib](http://matplotlib.org/): 2D-3D visualization, “publication-ready” plots.

With `IPython` and `Spyder`, Python plus these fundamental scientific packages
constitutes a very good alternative to Matlab, that is technically very similar (using
the libraries Blas and Lapack). Matlab has a JustInTime (JIT) compiler so that Matlab
code is generally faster than Python. However, we will see that Numpy is already quite
efficient for standard operations and other Python tools (for example `pypy`, `cython`,
`numba`, `pythran`, ...) can be used to optimize the code to reach the performance of
optimized Matlab code.

The advantage of Python over Matlab is its high polyvalency (and nicer syntax) and its
huge open-source ecosystem:

- [sympy](http://www.sympy.org) for symbolic computing,
- [pandas](http://pandas.pydata.org/), [statsmodels](http://www.statsmodels.org),
  [seaborn](http://seaborn.pydata.org/) for statistics,
- [h5py](http://www.h5py.org/), [h5netcdf](https://pypi.python.org/pypi/h5netcdf) for
  hdf5 and netcdf files,
- [mpi4py](https://pypi.python.org/pypi/mpi4py) for MPI communications,
- [opencv](https://pypi.python.org/pypi/opencv-python),
  [scikit-image](http://scikit-image.org/),
  [pyopencl](https://pypi.python.org/pypi/pyopencl) for image processing,
- [pycuda](https://mathema.tician.de/software/pycuda/) and
  [numba](https://numba.pydata.org/) for GPU computing,
- [scikit-learn](http://scikit-learn.org), [keras](https://keras.io/),
  [tensorflow](https://www.tensorflow.org/), [pytorch](https://pytorch.org/),
  [mxnet](http://mxnet.io/) for machine learning,
- [bokeh](http://bokeh.pydata.org) for display data efficiently,
- [mayavi](http://docs.enthought.com/mayavi/mayavi/) for 3D visualization,
- [qtpy](https://pypi.python.org/pypi/QtPy), [kivy](https://kivy.org) for GUI frameworks
- ...

```{admonition} SciPy or NumPy ?

`scipy` also provides a submodule for linear algebra `scipy.linalg`. It provides
an extension of `numpy.linalg`.

For more info, see the related FAQ entry:
https://www.scipy.org/scipylib/faq.html#why-both-numpy-linalg-and-scipy-linalg-what-s-the-difference.

```
