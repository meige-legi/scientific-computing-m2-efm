---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Manipulating NumPy arrays

```{code-cell} ipython3
import numpy as np
```

## Access or modify elements

Elements in a `numpy` array can be accessed using indexing and slicing in any dimension.
It also offers the same functionalities available in Fortan or Matlab.

### Indexes and slices

For example, we can create an array `A` and perform any kind of selection operations on
it.

```{code-cell} ipython3
A = np.random.random([4, 5])
A
```

```{code-cell} ipython3
# Get the element from second line, first column
A[1, 0]
```

```{code-cell} ipython3
# Get the first two lines
A[:2]
```

```{code-cell} ipython3
# Get the last column
A[:, -1]
```

```{code-cell} ipython3
# Get the first two lines and the columns with an even index
A[:2, ::2]
```

### Mask to select elements validating a condition

```{code-cell} ipython3
cond = A > 0.5
print(cond)
print(A[cond])
```

The mask is in fact a particular case of the advanced indexing capabilities provided by
NumPy. For example, it is even possible to use lists for indexing:

```{code-cell} ipython3
# Selecting only particular columns
print(A)
A[:, [0, 1, 4]]
```

## Perform array manipulations

### Apply arithmetic operations to whole arrays (element-wise)

```{code-cell} ipython3
(A + 5) ** 2
```

### Apply functions element-wise

```{code-cell} ipython3
np.exp(A)  # With numpy arrays, use the functions from numpy !
```

### Setting parts of arrays

```{code-cell} ipython3
A[:, 0] = 0.0
print(A)
```

```{code-cell} ipython3
# BONUS: Safe element-wise inverse with masks
cond = A != 0
A[cond] = 1.0 / A[cond]
print(A)
```

### Attributes and methods of `np.ndarray`

See the
[Numpy documentation](https://numpy.org/doc/stable/reference/generated/numpy.ndarray.html#numpy.ndarray).

```{code-cell} ipython3
print([s for s in dir(A) if not s.startswith("__")])
```

```{code-cell} ipython3
# Ex1: Get the mean through different dimensions
print(A)
print("Mean value", A.mean())
print("Mean line", A.mean(axis=0))
print("Mean column", A.mean(axis=1))
```

```{code-cell} ipython3
# Ex2: Convert a 2D array in 1D keeping all elements
print(A, A.shape)
A_flat = A.flatten()
print(A_flat, A_flat.shape)
```

### Dot product

```{code-cell} ipython3
b = np.linspace(0, 10, 11)
c = b @ b
# before 3.5:
# c = b.dot(b)
print(b)
print(c)
```

```{admonition} Comparison with Matlab

| ` `          | Matlab | Numpy |
| ------------ | ------ | ----- |
| element wise | `.*`   | `*`   |
| dot product  | `*`    | `@`   |

```

### `dtypes` and sub-packages

`numpy` arrays can also be sorted, even when they are composed of complex data if the
type of the columns are explicitly stated with `dtypes`.

```{code-cell} ipython3
dtypes = np.dtype(
    [("country", "S20"), ("density", "i4"), ("area", "i4"), ("population", "i4")]
)
x = np.array(
    [
        ("Netherlands", 393, 41526, 16928800),
        ("Belgium", 337, 30510, 11007020),
        ("United Kingdom", 256, 243610, 62262000),
        ("Germany", 233, 357021, 81799600),
    ],
    dtype=dtypes,
)
arr = np.array(x, dtype=dtypes)
arr.sort(order="density")
print(arr)
```

In the previous example, we manipulated a one dimensional array containing quadruplets
of data. This functionality can be used to load images into arrays and save arrays to
images.

It can also be used when loading data of different types from a file with
`np.genfromtxt`.

## NumPy and SciPy sub-packages:

### `numpy.random`

We already saw `numpy.random` to generate `numpy` arrays filled with random values. This
submodule also provides functions related to distributions (Poisson, gaussian, etc.) and
permutations.

### `numpy.linalg`

To perform linear algebra with dense matrices, we can use the submodule `numpy.linalg`.
For instance, in order to compute the determinant of a random matrix, we use the method
`det`

```{code-cell} ipython3
A = np.random.random([5, 5])
print(A)
np.linalg.det(A)
```

```{code-cell} ipython3
squared_subA = A[1:3, 1:3]
print(squared_subA)
np.linalg.inv(squared_subA)
```

If the data are sparse matrices, instead of using `numpy`, it is recommended to use
`scipy`.

```{code-cell} ipython3
from scipy.sparse import csr_matrix

print(csr_matrix([[1, 2, 0], [0, 0, 3], [4, 0, 5]]))
```
