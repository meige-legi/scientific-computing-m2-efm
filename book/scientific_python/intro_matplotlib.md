---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Introduction to Matplotlib

([gallery](http://matplotlib.org/gallery.html))

The default library to plot data is `Matplotlib`. It allows one the creation of graphs
that are ready for publications with the same functionality than Matlab.

```{code-cell} ipython3
# these ipython commands load special backend for notebooks
# (do not use "notebook" outside jupyter)
# %matplotlib notebook
# for jupyter-lab:
# %matplotlib ipympl
%matplotlib inline
```

When running code using matplotlib, it is highly recommended to start ipython with the
option `--matplotlib` (or to use the magic ipython command `%matplotlib`).

```{code-cell} ipython3
---
slideshow:
  slide_type: slide
---
import numpy as np
import matplotlib.pyplot as plt
```

```{code-cell} ipython3
A = np.random.random([5, 5])
```

You can plot any kind of numerical data.

```{code-cell} ipython3
lines = plt.plot(A)
```

In scripts, the `plt.show` method needs to be invoked at the end of the script.

We can plot data by giving specific coordinates.

```{code-cell} ipython3
x = np.linspace(0, 2, 20)
y = x**2
```

```{code-cell} ipython3
plt.figure()
plt.plot(x, y, label="Square function")
plt.xlabel("x")
plt.ylabel("y")
plt.legend()
```

We can associate the plot with an object figure. This object will allow us to add
labels, subplot, modify the axis or save it as an image.

```{code-cell} ipython3
fig = plt.figure()
ax = fig.add_subplot(111)
res = ax.plot(
    x,
    y,
    color="red",
    linestyle="dashed",
    linewidth=3,
    marker="o",
    markerfacecolor="blue",
    markersize=5,
)

ax.set_xlabel("$Re$")
ax.set_ylabel("$\Pi / \epsilon$")
```

We can also recover the plotted matplotlib object to get info on it.

```{code-cell} ipython3
line_object = res[0]
print(type(line_object))
print("Color of the line is", line_object.get_color())
print("X data of the plot:", line_object.get_xdata())
```

## Example of multiple subplots

```{code-cell} ipython3
fig = plt.figure()
ax1 = fig.add_subplot(
    211
)  # First, number of subplots along X (2), then along Y (1), then the id of the subplot (1)
ax2 = fig.add_subplot(212, sharex=ax1)  # It is possible to share axes between subplots
X = np.arange(0, 2 * np.pi, 0.1)
ax1.plot(X, np.cos(2 * X), color="red")
ax2.plot(X, np.sin(2 * X), color="magenta")
ax2.set_xlabel("Angle (rad)")
```

## Anatomy of a Matplotlib figure

![Anatomy of a figure](./anatomy.png)

For consistent figure changes, define your own stylesheets that are basically a list of
parameters to tune the aspect of the figure elements. See
https://matplotlib.org/tutorials/introductory/customizing.html for more info.

```{code-cell} ipython3

```

We can also plot 2D data arrays.

```{code-cell} ipython3
noise = np.random.random((256, 256))
plt.figure()
plt.imshow(noise)
```

We can also add a colorbar and adjust the colormap.

```{code-cell} ipython3
plt.figure()
plt.imshow(noise, cmap=plt.cm.gray)
plt.colorbar()
```

### Choose your colormaps wisely !

When doing such colorplots, it is easy to lose the interesting features by setting a
colormap that is not adapted to the data.

Also, when producing scientific figures, think about how will your plot will look like
to colorblind people or in greyscales (as it can happen in printed articles...).

See the interesting discussion on matplotlib website:
https://matplotlib.org/users/colormaps.html.

## Other plot types

Matplotlib also allows to plot:

- Histograms
- Plots with error bars
- Box plots
- Contours
- in 3D
- ...

See the [gallery](http://matplotlib.org/gallery.html) to see what suits you the most.
