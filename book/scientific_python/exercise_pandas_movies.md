---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Exercise with Pandas: movies

Pandas is an open source library providing high-performance, easy-to-use data structures
and data analysis tools for Python.

Goals : Compute light statistics on IMDB Movies files

The goal of this session is to end up with a script that computes some simple statistics
from IMDB Movies files. The file was modified and reduced for this exercice

Material

Data are in 2 files Directory named "files"

- name.tsv

This file contains the actors, the separation character is tabulation '\\t'. The first
line is the header.

```
nconst	primaryName	birthYear	deathYear	primaryProfession	knownForTitles
```

- title.tsv

This file contains the movies, the separation character is ','. The first line is the
header.

```
tconst,titleType,primaryTitle,originalTitle,isAdult,startYear,endYear,runtimeMinutes,genres
```

We want to

- load data from tsv file
- compute some basic statistics
- save data to tsv file

## Compute some basic statistics

1. Count the number of movies
2. Display the latest movies
3. Display the movies between 1939 and 1940
4. Diplay all the available titleType
5. Count the number of movies by titleType
6. Display Humphrey Bogart movies
7. Plot movie count by year between 1950 and 1960

## A possible correction

1. Count the number of movies

```{code-cell} ipython3
import pandas as pd

# load data
df_title = pd.read_csv("data_movies/title.tsv", sep=",", encoding="utf-8", header=0)
df_title.index
```

```{code-cell} ipython3
df_title.head()
```

```{code-cell} ipython3
df_title.primaryTitle.count()
```

2. Display the latest movies

```{code-cell} ipython3
df_title.sort_values(["startYear"], ascending=False).head()
```

3. Display the movies between 1939 and 1940

```{code-cell} ipython3
df_title[(df_title.startYear >= 1939) & (df_title.startYear <= 1940)]["originalTitle"]
```

4. Diplay all the available titleType

```{code-cell} ipython3
df_title["titleType"].unique()
```

5. Count the number of movies by titleType

```{code-cell} ipython3
df_title.groupby("titleType")["titleType"].count()
```

6. Display Humphrey Bogart movies

```{code-cell} ipython3
df_name = pd.read_csv("data_movies/name.tsv", sep="\t", encoding="utf-8", header=0)
author_titles = df_name.loc[df_name["primaryName"] == "Humphrey Bogart"][
    "knownForTitles"
]
df_title.loc[df_title["tconst"].isin(author_titles.tolist()[0].split(","))]
```

```{code-cell} ipython3
df = (
    df_title[(df_title["startYear"] >= 1950) & (df_title["startYear"] <= 1960)]
    .groupby("startYear")["startYear"]
    .count()
)
df
```

7. Plot movie count by year between 1950 and 1960

```{code-cell} ipython3
df.plot.bar()
```
