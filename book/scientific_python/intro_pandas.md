---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Introduction to Pandas

**Webography**

- [Pandas tutorial](https://pandas.pydata.org/pandas-docs/stable/10min.html)

- [Grenoble Python Working Session](https://github.com/iutzeler/Pres_Pandas/)

- [Pandas for SQL Users](https://hackernoon.com/pandas-cheatsheet-for-sql-people-part-1-2976894acd0)

- Online Doc: https://pandas.pydata.org/

- CheatSheet : https://pandas.pydata.org/Pandas_Cheat_Sheet.pdf

- Tutorials:

  - https://www.dataschool.io/easier-data-analysis-with-pandas/
  - https://www.dataschool.io/data-science-best-practices-with-pandas/

- Pandas PySciDataGre Talk :
  https://python.univ-grenoble-alpes.fr/working-session-librairie-pandas.html

- GeoPandas : http://geopandas.org/

Code using pandas usually starts with the import statement

```{code-cell} ipython3
import numpy as np
import pandas as pd
```

Pandas

- 2 data structures (Series, DataFrame) for data analysis

- multiple methods for convenient data filtering.

- toolkit utilities to perform input/output operations. It can read data from a variety
  of formats such as CSV, TSV, MS Excel, etc.

Pandas has two main data structures for data storage

- Series
- DataFrame

```{code-cell} ipython3
---
slideshow:
  slide_type: slide
---
## Series structure
series1 = pd.Series([1, 2, 3, 4])
series1
```

```{code-cell} ipython3
print(series1.sum())
print(series1.mean())
```

```{code-cell} ipython3
print(series1.to_csv())
```

```{code-cell} ipython3
---
slideshow:
  slide_type: slide
---
fruits = np.array(["kiwi", "orange", "mango", "apple"])
series2 = pd.Series(fruits)
series2
```

# Dataframe

A dictionnary of series where keys are column name

```{image} ./dataframe_type.png
---
alt: dataframe_type
width: 95%
align: center
---
```

## How to create a data frame ?

### From scratch

```{code-cell} ipython3
# Intialise data: dictionnary of lists.
data = {
    "Name": ["John", "Paul", "Debby", "Laura"],
    "Sex": ["Male", "Male", "Female", "Female"],
    "Age": [20, 40, 19, 30],
}

# Create DataFrame
df = pd.DataFrame(data)
df
```

```{code-cell} ipython3
type(df.Age)
```

```{code-cell} ipython3
df.to_csv("person.txt")
```

### From a file

```{code-cell} ipython3
df_person = pd.read_csv("person.txt", sep=",", encoding="utf-8", header=0)
df_person
```

By default, a new index is created

If you want use a field-based index, you have to specify it in the `read_csv` function:

```{code-cell} ipython3
df_person = pd.read_csv('person.txt', sep = ',', index_col=0, encoding="utf-8", header=0)
df_person
```

## Basic commands

```{code-cell} ipython3
---
tags: []
---
# display simple statistics
df_person.describe()
```

```{code-cell} ipython3
---
tags: []
---
# display the 5 first rows
df_person.head()
```

```{code-cell} ipython3
---
tags: []
---
# display the 5 last rows
df_person.tail()
```

```{code-cell} ipython3
---
slideshow:
  slide_type: slide
---
# display the dataframe columns
df_person.columns
```

```{code-cell} ipython3
---
slideshow:
  slide_type: slide
---
# query one column
df_person["Age"]
```

```{code-cell} ipython3
# another method to query one column
df_person.Age
```

```{code-cell} ipython3
---
tags: []
---
# query multiple columns
df_person[["Name", "Age"]]
```

```{code-cell} ipython3
---
slideshow:
  slide_type: slide
---
# display unique value of a column
df_person.Sex.unique()
```

```{code-cell} ipython3
---
slideshow:
  slide_type: slide
---
# display 2 first rows
df_person[:2]
```

### iloc: Purely integer-location based indexing for selection by position.

```{code-cell} ipython3
df_person.head()
```

```{code-cell} ipython3
---
tags: []
---
df_person.iloc[2]
```

```{code-cell} ipython3
df_person.iloc[2, 2]
```

### `loc`: access a group of rows and columns by label(s) or a boolean array.

```{code-cell} ipython3
df_person.head()
```

```{code-cell} ipython3
# one line
df_person.loc[2]
```

```{code-cell} ipython3
# one value
df_person.loc[2, "Name"]
```

### Basic operations on columns

```{code-cell} ipython3
df_person.Age = df_person.Age + 2
df_person.Age
```

### Get or set a single value (fast)

- `df_person.at`: by labels

- `df_person.iat`: "integer-location based indexing"

```{code-cell} ipython3
df_person.head()
```

```{code-cell} ipython3
df_person.at[2, "Name"]
```

```{code-cell} ipython3
df_person.iat[2, 0]
```

### Add a row

```{code-cell} ipython3
new_row = {"Name": "Glenn", "Sex": "Male", "Age": 10}
df_person = pd.concat([df_person, pd.DataFrame([new_row])], ignore_index=True)
df_person
```

### Add some rows

```{code-cell} ipython3
data = {
    "Name": ["Marguerite", "Annie", "Stephen", "Ava"],
    "Sex": ["Female", "Female", "Male", "Female"],
    "Age": [34, 23, 49, 22],
}
df_person = pd.concat([df_person, pd.DataFrame(data)], ignore_index=True)
df_person
```

### Add a column

```{code-cell} ipython3
df_person["Nationality"] = "USA"
df_person
```

## Basic statistics

```{code-cell} ipython3
type(df_person.Age)
```

```{code-cell} ipython3
print(df_person.Age.mean())
print(df_person.Age.min())
print(df_person.Age.max())
print(df_person.Age.count())
```

## How to sort data ?

```{code-cell} ipython3
df_person_sorted = df_person.sort_values(["Age"], ascending=True)
df_person_sorted
```

## Selection

```{code-cell} ipython3
# selection with one criterion
df_person[df_person["Sex"] == "Female"]
```

```{code-cell} ipython3
df_person[df_person["Age"] < 20]
```

```{code-cell} ipython3
# selection with 2 criteria
df_person[(df_person["Sex"] == "Male") & (df_person["Age"] > 30)]
```

## Update data

```{code-cell} ipython3
# change one value by index
df_person.loc[7, "Name"] = "Stephane"
df_person
```

```{code-cell} ipython3
---
slideshow:
  slide_type: slide
---
# change one value after a selection
df_person.loc[df_person["Name"] == "Stephane", "Name"] = "Eric"
df_person
```

```{code-cell} ipython3
---
slideshow:
  slide_type: slide
---
## Add a column
df_person["City"] = "City"
df_person
```

```{code-cell} ipython3
---
slideshow:
  slide_type: slide
---
## Delete a column
df_person = df_person.drop("City", axis=1)
df_person
```

## Concat

```{image} fig/concat-example.png
---
alt: concat-example
width: 80%
align: center
---
```

```{code-cell} ipython3
data = {
    "Name": ["Benedicte", "Bernard", "Nicolas", "Anne"],
    "Sex": ["Female", "Male", "Male", "Female"],
    "Age": [24, 34, 49, 42],
    "Nationality": ["FR", "FR", "FR", "FR"],
}
df_person_fr = pd.DataFrame(data)
list_person = [df_person, df_person_fr]
result = pd.concat(list_person)
result
```

## Join

```{image} fig/join-example.png
---
alt: join-example
width: 80%
align: center
---
```

```{code-cell} ipython3
---
slideshow:
  slide_type: slide
---
import random

data = {
    "id_Address": [0, 1, 2, 3],
    "Address": ["gordon street", "aqua boulevard", "st georges street", "5th street"],
    "City": ["Boston", "Chicago", "Charlotte", "San Francisco"],
}

# Create DataFrame
df_address = pd.DataFrame(data)
df_address
```

```{code-cell} ipython3
---
slideshow:
  slide_type: slide
---
df_person["id_Address"] = ""
nb_elements = df_person.Name.count()

cpt = 0
while cpt < nb_elements:
    df_person.loc[cpt, "id_Address"] = random.randint(0, 3)
    cpt = cpt + 1
df_person
```

```{code-cell} ipython3
---
slideshow:
  slide_type: slide
---
result = pd.merge(df_person, df_address, how="left", on="id_Address")
result
```

## Group By

- Splitting the data into groups based on some criteria.
- Applying a function to each group independently.
- Combining the results into a data structure.

```{image} fig/groupby-example.png
---
alt: groupby-example
width: 80%
align: center
---
```

```{code-cell} ipython3
---
slideshow:
  slide_type: slide
---
df_person.groupby("Sex")["Sex"].count()
```

```{code-cell} ipython3
df_person.groupby("Sex")["Age"].mean()
```

## Export data

```{code-cell} ipython3
export_csv = df_person.to_csv(r"./export_person.csv", index=None, header=True)
```

## Plot data

```{code-cell} ipython3
%matplotlib inline
df_person.groupby("Sex")["Sex"].count().plot.bar();
```
