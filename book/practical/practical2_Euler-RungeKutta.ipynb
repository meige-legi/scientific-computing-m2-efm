{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Euler and Runge Kutta Methods"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The main objective is to investigate two methods -- the Euler method and the second order Runge-Kutta method -- allowing to solve numerically a Cauchy problem which has the form:\n",
    "\n",
    "\\begin{align}\n",
    "y' &= f(y) \\\\\n",
    "y(0) &= \\alpha\n",
    "\\end{align}\n",
    "\n",
    "\n",
    "To do so, we will focus on the problem of the thermal cooling or heating of a body which is in contact with an environment at a temperature different than its own. This problem is governed by\n",
    "the following differential equation:\n",
    "\n",
    "\\begin{equation}\n",
    "\\frac{dT(t)}{dt}=\\kappa \\left(T_{amb} - T(t) \\right)\n",
    "\\end{equation}\n",
    "\n",
    "This equation is also called \"Newton's cooling law\", where $T(t)$ is the body temperature as a function\n",
    "of time, $T_{amb}$ the assumed constant ambient temperature and $\\kappa$ a proportionality constant of units $[s^{-1}]$.\n",
    "\n",
    "For a question of clarity within this notebook (in both markdown and code cells), $T$ will refer to the Temperature when $t$ will refer to the time."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's consider the following Cauchy problem:\n",
    "\n",
    "\\begin{align}\n",
    "\\frac{dT(t)}{dt} &= \\kappa (T_{amb}-T(t)) \\\\\n",
    "T(t=0) &= T_0\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "where:\n",
    "\\begin{align*}\n",
    "T_{amb} &= 20 \\; [°C] \\\\\n",
    "T_0 &= 10 \\; [°C] \\\\\n",
    "\\kappa &= 0.2 \\; [s^{-1}] \\\\\n",
    "t_{max} &= 50 \\; s \\\\\n",
    "\\end{align*}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "0. **Define the constants of the problems for the rest of the session**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Fix a set of parameters\n",
    "T0 = 10  # Initial temperature\n",
    "tmax = 50  # Time up to which perform the simulation\n",
    "kappa = 0.2  # Constant of the problem\n",
    "Tamb = 20  # Temperature of the environment"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "1. **Show that the exact solution is the one shown below.**\n",
    "\n",
    "\\begin{equation}\n",
    "T_s = T_{amb} + (T_0 - T_{amb}) \\times e^{-\\kappa t}\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2. **Define a function corresponding to the right hand side of the Newton's cooling law.**\n",
    "\n",
    "<div class=\"alert alert-block alert-info\">\n",
    "<b>Note:</b> When creating a function, remember to include explicit documentation on what it does and what it needs as input data.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "def NCL(T, kappa=kappa, Tamb=Tamb):\n",
    "    \"\"\"\n",
    "    Function which returns the right hand side of Newton's Cooling Law\n",
    "\n",
    "    Parameters:\n",
    "    T : float\n",
    "        Actual temperature\n",
    "    Tamb : float\n",
    "        Ambiant temperature\n",
    "    kappa : float\n",
    "        Temprature change rate\n",
    "\n",
    "    Output:\n",
    "        float\n",
    "\n",
    "    \"\"\"\n",
    "    return None"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Euler's method\n",
    "\n",
    "The Euler's method can be easily understood starting from the differential operator:\n",
    "\n",
    "\\begin{equation}\n",
    "y' \\left( t \\right) \\approx \\frac{y(t+\\delta t) - y(t)}{\\delta t}\n",
    "\\end{equation}\n",
    "\n",
    "Which can be re-written as:\n",
    "\n",
    "\\begin{equation}\n",
    "y(t+\\delta t) \\approx y(t) + \\delta t \\times y' \\left( t \\right)\n",
    "\\end{equation}\n",
    "\n",
    "Then, using the property of a Cauchy problem $y'=f(y)$ one can finally obtain:\n",
    "\n",
    "\\begin{equation}\n",
    "y(t+\\delta t) \\approx y(t) + \\delta t \\times f \\left( y \\left( t \\right) \\right)\n",
    "\\end{equation}\n",
    "\n",
    "Since the initial condition is known from the problem's definition, one can compute from the previous formula an approximation of $y(\\delta t)$. Therefore, it is possible to process iteratively and obtain an approximated value of $y(t_{max})$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "3. **Now complete the function below so that it applies the Euler method**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "def Euler(alpha, tmax, dt, func):\n",
    "    \"\"\"Function to integrate a Cauchy problem of the following form\n",
    "                    y'=func(y) & y(0)=alpha\n",
    "    from 0 to tmax with a step dt.\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    T0 : float\n",
    "        Initial temperature of the Cauchy problem\n",
    "    tmax : float\n",
    "        Value to be reched by the integration\n",
    "    dt : float\n",
    "        Integration timestep\n",
    "    func  : python function\n",
    "        Right Hand Side of the Cauchy problem\n",
    "\n",
    "    Output\n",
    "    ----------\n",
    "    1D numpy.array\n",
    "        Discretization on which approximated values have been computed\n",
    "    1D numpy.array\n",
    "        Values of the approximated solution on the output discretization\n",
    "    \"\"\"\n",
    "    # Preliminary computations\n",
    "    N = int(tmax / dt)  # Number of iterations to get from 0 to tmax with time step dt\n",
    "    y = np.zeros(N + 1)  # Vector to store the results.\n",
    "    t = np.zeros(\n",
    "        N + 1\n",
    "    )  # Vector to store the time discretization (useful for plotting).\n",
    "\n",
    "    # Intialization\n",
    "    # y[0] =\n",
    "    # t[0] =\n",
    "\n",
    "    # Euler's method\n",
    "    return t, y"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "4. **In order to assess the capability of our numerical method it is also necessary to compare it to the exact solution. Complete the function below to compute the exact solution according to question 1.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "def Tex(T0, t, Tamb=Tamb, kappa=kappa):\n",
    "    \"\"\"Function that returns the solution to the Newton's Cooling law\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    T0 : float\n",
    "        Initial temperature for the Cauchy problem\n",
    "    t  : numpy.array\n",
    "        Vector of time at which to compute the exact solution\n",
    "    Tamb : float\n",
    "        Ambient temperature for the Cauchy problem\n",
    "    kappa : float\n",
    "\n",
    "\n",
    "    Output\n",
    "    ----------\n",
    "    float or numpy.array (1D)\n",
    "        Exact solution to the Newton's cooling law problem\n",
    "    \"\"\"\n",
    "    sol = None\n",
    "    return sol"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "5. **Make a function using matplotlib.pyplot to plot the exact and computed solution of the cauchy problem.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_approx_vs_orig(xapprox, yapprox, xexact, yexact):\n",
    "    \"\"\"Minimal function to compare visually an exact solution and\n",
    "    an approximated solution to a 1D problem.\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    xapprox : numpy.array (1D)\n",
    "        Discretization of the approximated solution\n",
    "    yapprox : numpy.array (1D)\n",
    "        Values of the approximated solution\n",
    "    xexact : numpy.array (1D)\n",
    "        Discretization of the exact solution\n",
    "    yexact : numpy.array (1D)\n",
    "        Values of the exact solution\n",
    "\n",
    "    Output\n",
    "    ----------\n",
    "    None :\n",
    "        No output but it will display a graphic using plt.show()\n",
    "    \"\"\"\n",
    "    fig, ax = plt.subplots()\n",
    "    # Complete here\n",
    "    plt.show()\n",
    "    return None"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "5. **Use the Euler's method to solve the problem with different time steps. Among these, identify time steps for which we obtain the following behaviors:**\n",
    "    1. **Divergence of the solution: solution going towards $\\infty$ or $-\\infty$**\n",
    "    2. **The solution does not converge towards the exact solution without reaching $\\pm \\infty$**\n",
    "    3. **The solution looks alike the exact solution**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAXwAAAD8CAYAAAB0IB+mAAAAOXRFWHRTb2Z0d2FyZQBNYXRwbG90bGliIHZlcnNpb24zLjMuMiwgaHR0cHM6Ly9tYXRwbG90bGliLm9yZy8vihELAAAACXBIWXMAAAsTAAALEwEAmpwYAAANQklEQVR4nO3cX4il9X3H8fenuxEak0aJk5DurmRb1pi90KITI6VpTUObXXuxBLxQQ6QSWKQx5FIpNLnwprkohKBmWWSR3GQvGkk2ZRMplMSCNd1Z8N8qynSlOl3BNYYUDFRWv704p51hnHWenXNmZp3v+wUD85znNzPf+TH73mfPznlSVUiStr7f2ewBJEkbw+BLUhMGX5KaMPiS1ITBl6QmDL4kNbFq8JMcSfJakmfPcz5JvptkPsnTSa6b/piSpEkNucJ/GNj3Huf3A3vGbweB700+liRp2lYNflU9BrzxHksOAN+vkSeAy5J8YloDSpKmY/sUPscO4JUlxwvjx15dvjDJQUb/CuDSSy+9/uqrr57Cl5ekPk6ePPl6Vc2s5WOnEfys8NiK92uoqsPAYYDZ2dmam5ubwpeXpD6S/OdaP3Yav6WzAOxacrwTODOFzytJmqJpBP8YcMf4t3VuBH5TVe96OkeStLlWfUonyQ+Am4ArkiwA3wI+AFBVh4DjwM3APPBb4M71GlaStHarBr+qblvlfAFfm9pEkqR14SttJakJgy9JTRh8SWrC4EtSEwZfkpow+JLUhMGXpCYMviQ1YfAlqQmDL0lNGHxJasLgS1ITBl+SmjD4ktSEwZekJgy+JDVh8CWpCYMvSU0YfElqwuBLUhMGX5KaMPiS1ITBl6QmDL4kNWHwJakJgy9JTRh8SWrC4EtSEwZfkpow+JLUhMGXpCYMviQ1YfAlqQmDL0lNGHxJamJQ8JPsS/JCkvkk965w/iNJfpLkqSSnktw5/VElSZNYNfhJtgEPAPuBvcBtSfYuW/Y14Lmquha4CfiHJJdMeVZJ0gSGXOHfAMxX1emqegs4ChxYtqaADycJ8CHgDeDcVCeVJE1kSPB3AK8sOV4YP7bU/cCngTPAM8A3quqd5Z8oycEkc0nmzp49u8aRJUlrMST4WeGxWnb8ReBJ4PeBPwLuT/J77/qgqsNVNVtVszMzMxc4qiRpEkOCvwDsWnK8k9GV/FJ3Ao/UyDzwEnD1dEaUJE3DkOCfAPYk2T3+j9hbgWPL1rwMfAEgyceBTwGnpzmoJGky21dbUFXnktwNPApsA45U1akkd43PHwLuAx5O8gyjp4DuqarX13FuSdIFWjX4AFV1HDi+7LFDS94/A/zldEeTJE2Tr7SVpCYMviQ1YfAlqQmDL0lNGHxJasLgS1ITBl+SmjD4ktSEwZekJgy+JDVh8CWpCYMvSU0YfElqwuBLUhMGX5KaMPiS1ITBl6QmDL4kNWHwJakJgy9JTRh8SWrC4EtSEwZfkpow+JLUhMGXpCYMviQ1YfAlqQmDL0lNGHxJasLgS1ITBl+SmjD4ktSEwZekJgy+JDUxKPhJ9iV5Icl8knvPs+amJE8mOZXkF9MdU5I0qe2rLUiyDXgA+AtgATiR5FhVPbdkzWXAg8C+qno5ycfWaV5J0hoNucK/AZivqtNV9RZwFDiwbM3twCNV9TJAVb023TElSZMaEvwdwCtLjhfGjy11FXB5kp8nOZnkjpU+UZKDSeaSzJ09e3ZtE0uS1mRI8LPCY7XseDtwPfBXwBeBv0ty1bs+qOpwVc1W1ezMzMwFDytJWrtVn8NndEW/a8nxTuDMCmter6o3gTeTPAZcC7w4lSklSRMbcoV/AtiTZHeSS4BbgWPL1vwY+FyS7Uk+CHwWeH66o0qSJrHqFX5VnUtyN/AosA04UlWnktw1Pn+oqp5P8jPgaeAd4KGqenY9B5ckXZhULX86fmPMzs7W3NzcpnxtSXq/SnKyqmbX8rG+0laSmjD4ktSEwZekJgy+JDVh8CWpCYMvSU0YfElqwuBLUhMGX5KaMPiS1ITBl6QmDL4kNWHwJakJgy9JTRh8SWrC4EtSEwZfkpow+JLUhMGXpCYMviQ1YfAlqQmDL0lNGHxJasLgS1ITBl+SmjD4ktSEwZekJgy+JDVh8CWpCYMvSU0YfElqwuBLUhMGX5KaMPiS1ITBl6QmBgU/yb4kLySZT3Lve6z7TJK3k9wyvRElSdOwavCTbAMeAPYDe4Hbkuw9z7pvA49Oe0hJ0uSGXOHfAMxX1emqegs4ChxYYd3XgR8Cr01xPknSlAwJ/g7glSXHC+PH/l+SHcCXgEPv9YmSHEwyl2Tu7NmzFzqrJGkCQ4KfFR6rZcffAe6pqrff6xNV1eGqmq2q2ZmZmYEjSpKmYfuANQvAriXHO4Ezy9bMAkeTAFwB3JzkXFX9aBpDSpImNyT4J4A9SXYD/wXcCty+dEFV7f6/95M8DPyTsZeki8uqwa+qc0nuZvTbN9uAI1V1Ksld4/Pv+by9JOniMOQKn6o6Dhxf9tiKoa+qv558LEnStPlKW0lqwuBLUhMGX5KaMPiS1ITBl6QmDL4kNWHwJakJgy9JTRh8SWrC4EtSEwZfkpow+JLUhMGXpCYMviQ1YfAlqQmDL0lNGHxJasLgS1ITBl+SmjD4ktSEwZekJgy+JDVh8CWpCYMvSU0YfElqwuBLUhMGX5KaMPiS1ITBl6QmDL4kNWHwJakJgy9JTRh8SWrC4EtSE4OCn2RfkheSzCe5d4XzX07y9Pjt8STXTn9USdIkVg1+km3AA8B+YC9wW5K9y5a9BPxZVV0D3AccnvagkqTJDLnCvwGYr6rTVfUWcBQ4sHRBVT1eVb8eHz4B7JzumJKkSQ0J/g7glSXHC+PHzuerwE9XOpHkYJK5JHNnz54dPqUkaWJDgp8VHqsVFyafZxT8e1Y6X1WHq2q2qmZnZmaGTylJmtj2AWsWgF1LjncCZ5YvSnIN8BCwv6p+NZ3xJEnTMuQK/wSwJ8nuJJcAtwLHli5IciXwCPCVqnpx+mNKkia16hV+VZ1LcjfwKLANOFJVp5LcNT5/CPgm8FHgwSQA56pqdv3GliRdqFSt+HT8upudna25ublN+dqS9H6V5ORaL6h9pa0kNWHwJakJgy9JTRh8SWrC4EtSEwZfkpow+JLUhMGXpCYMviQ1YfAlqQmDL0lNGHxJasLgS1ITBl+SmjD4ktSEwZekJgy+JDVh8CWpCYMvSU0YfElqwuBLUhMGX5KaMPiS1ITBl6QmDL4kNWHwJakJgy9JTRh8SWrC4EtSEwZfkpow+JLUhMGXpCYMviQ1YfAlqQmDL0lNDAp+kn1JXkgyn+TeFc4nyXfH559Oct30R5UkTWLV4CfZBjwA7Af2Arcl2bts2X5gz/jtIPC9Kc8pSZrQkCv8G4D5qjpdVW8BR4EDy9YcAL5fI08AlyX5xJRnlSRNYPuANTuAV5YcLwCfHbBmB/Dq0kVJDjL6FwDA/yR59oKm3bquAF7f7CEuEu7FIvdikXux6FNr/cAhwc8Kj9Ua1lBVh4HDAEnmqmp2wNff8tyLRe7FIvdikXuxKMncWj92yFM6C8CuJcc7gTNrWCNJ2kRDgn8C2JNkd5JLgFuBY8vWHAPuGP+2zo3Ab6rq1eWfSJK0eVZ9SqeqziW5G3gU2AYcqapTSe4anz8EHAduBuaB3wJ3Dvjah9c89dbjXixyLxa5F4vci0Vr3otUveupdknSFuQrbSWpCYMvSU2se/C9LcOiAXvx5fEePJ3k8STXbsacG2G1vViy7jNJ3k5yy0bOt5GG7EWSm5I8meRUkl9s9IwbZcCfkY8k+UmSp8Z7MeT/C993khxJ8tr5Xqu05m5W1bq9MfpP3v8A/gC4BHgK2Ltszc3ATxn9Lv+NwC/Xc6bNehu4F38MXD5+f3/nvViy7l8Y/VLALZs99yb+XFwGPAdcOT7+2GbPvYl78bfAt8fvzwBvAJds9uzrsBd/ClwHPHue82vq5npf4XtbhkWr7kVVPV5Vvx4fPsHo9Qxb0ZCfC4CvAz8EXtvI4TbYkL24HXikql4GqKqtuh9D9qKADycJ8CFGwT+3sWOuv6p6jNH3dj5r6uZ6B/98t1y40DVbwYV+n19l9Df4VrTqXiTZAXwJOLSBc22GIT8XVwGXJ/l5kpNJ7tiw6TbWkL24H/g0oxd2PgN8o6re2ZjxLipr6uaQWytMYmq3ZdgCBn+fST7PKPh/sq4TbZ4he/Ed4J6qent0MbdlDdmL7cD1wBeA3wX+LckTVfXieg+3wYbsxReBJ4E/B/4Q+Ock/1pV/73Os11s1tTN9Q6+t2VYNOj7THIN8BCwv6p+tUGzbbQhezELHB3H/grg5iTnqupHGzLhxhn6Z+T1qnoTeDPJY8C1wFYL/pC9uBP4+xo9kT2f5CXgauDfN2bEi8aaurneT+l4W4ZFq+5FkiuBR4CvbMGrt6VW3Yuq2l1Vn6yqTwL/CPzNFow9DPsz8mPgc0m2J/kgo7vVPr/Bc26EIXvxMqN/6ZDk44zuHHl6Q6e8OKypm+t6hV/rd1uG952Be/FN4KPAg+Mr23O1Be8QOHAvWhiyF1X1fJKfAU8D7wAPVdWWu7X4wJ+L+4CHkzzD6GmNe6pqy902OckPgJuAK5IsAN8CPgCTddNbK0hSE77SVpKaMPiS1ITBl6QmDL4kNWHwJakJgy9JTRh8SWrifwHXe3WluIZOawAAAABJRU5ErkJggg==\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "# Compute the exact solution once\n",
    "disc_ex = np.linspace(0, tmax)\n",
    "exact = Tex(T0, disc_ex)\n",
    "\n",
    "# Loop over a set of time steps to illustrate the 3 scenarios\n",
    "dts = [1]\n",
    "for dt in dts:\n",
    "    disc, approx = Euler(T0, tmax, dt, NCL)\n",
    "    plot_approx_vs_orig(disc, approx, disc_ex, exact)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "6. **For a list of time steps belonging to case (5C) compute the maximal numerical error according to the formula:**\n",
    "\n",
    "\\begin{equation}\n",
    "\\epsilon(dt) = \\max_{0 \\le t \\leq t_{max}}\\left( \\left| T_{num}(t) - T_{ex}(t)\\right| \\right)\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[  0. -10.   0.   0.  16.   0.  16. -10.]\n"
     ]
    }
   ],
   "source": [
    "# Example set of time steps\n",
    "dts = np.array([2, 1, 0.5, 0.1, 0.05, 0.01, 0.005, 0.001])\n",
    "errors = np.empty(dts.shape)\n",
    "\n",
    "# Compute errors\n",
    "\n",
    "print(errors)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "7. **Plot on a log-log scale the error $\\epsilon$ as a function of the time step $dt$ used in 6.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Make figure here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "8. **You should observe that the points are aligned. Fit the points using numpy.polyfit to get the slope of the line. Remember that polyfit uses polynomial models of order n such as $p(x)=P[n] + P[n-1] x^1 + \\cdots + P[0] x^n$. Print the results to the screen.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "9. **To check that everything went all right, copy the plot of the previous question and verify that this model and the points do superimpose.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Runge Kutta of order 2\n",
    "\n",
    "In a similar fashion as for the Euler's method, one can write from the differential operator:\n",
    "\n",
    "\\begin{equation}\n",
    "y'\\left(t+\\frac{dt}{2} \\right) \\approx \\frac{y(t+dt) - y(t)}{dt}\n",
    "\\end{equation}\n",
    "\n",
    "Which, once re-written yields to\n",
    "\n",
    "\\begin{equation}\n",
    "y(t+dt) \\approx y(t) + dt \\times y'\\left(t+\\frac{dt}{2} \\right)\n",
    "\\end{equation}\n",
    "\n",
    "Using the definition of a Cauchy problem ($y'=f(y)$):\n",
    "\n",
    "\\begin{equation}\n",
    "y(t+dt) \\approx y(t) + dt \\times f \\left( y\\left(t+\\frac{dt}{2} \\right) \\right)\n",
    "\\end{equation}\n",
    "\n",
    "This formula can not be used straight away because $y\\left(t+\\frac{dt}{2} \\right)$ is not known. However it is possible to compute it first from the Euler's formula:\n",
    "\n",
    "\\begin{equation}\n",
    "y \\left( t+\\frac{dt}{2} \\right) \\approx y(t) + \\frac{dt}{2} \\times f \\left( y\\left(t \\right) \\right)\n",
    "\\end{equation}\n",
    "\n",
    "And then to proceed to the computation yielding to the result.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "9. **Do the same study with the Runge Kutta Method as the one you performed with the Euler's method (Code the method, explore the behaviour of the method and finally compute the convergence order).**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "def RK2(alpha, tmax, dt, function):\n",
    "    \"\"\"Function to integrate a Cauchy problem of the following form\n",
    "                    y'=func(y) & y(0)=alpha\n",
    "    from 0 to tmax with a step dt using the Runge Kutta of order 2 scheme.\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    alpha : float\n",
    "        Initialization of the cauchy problem\n",
    "    tmax : float\n",
    "        Final value to be reached by the integration\n",
    "    dt : float\n",
    "        Step at which to go forward in the integrating direction\n",
    "    function  : python function\n",
    "        Right Hand Side of the Cauchy problem\n",
    "\n",
    "    Output\n",
    "    ----------\n",
    "    1D numpy.array\n",
    "        Discretization on which approximated values have been computed\n",
    "    1D numpy.array\n",
    "        Values of the approximated solution on the output discretization\n",
    "    \"\"\"\n",
    "    # Complete the function so that it does what the function description explains.\n",
    "    return None, None"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "10. **Use the Euler's method to solve the problem with different time steps. Among these, identify time steps for which we obtain the following behaviors:**\n",
    "    1. **Divergence of the solution: solution going towards $\\infty$ or $-\\infty$**\n",
    "    2. **The solution does not converge towards the exact solution without reaching $\\pm \\infty$**\n",
    "    3. **The solution looks alike the exact solution**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "11. **For a list of time steps belonging to case (5C) compute the maximal numerical error according to the formula:**\n",
    "\n",
    "\\begin{equation}\n",
    "\\epsilon(dt) = \\max_{0 \\le t \\leq t_{max}}\\left( \\left| T_{num}(t) - T_{ex}(t)\\right| \\right)\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "12. **Plot on a log-log scale the error $\\epsilon$ as a function of the time step $dt$ used in 11.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "13. **You should observe that the points are aligned. Fit the points using numpy.polyfit to get the slope of the line. Print the results.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "14. **To check that everything went all right, copy the plot of the previous question and verify that this model and the points do superimpose.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Compare Euler and RK methods"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "15. **According to what you have done, which method appears to be the best and why ?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Built-in functions\n",
    "Similarly as for integration methods, built-in functions for solving ODEs exist in python. One can use _odeint_ in _scipy_ to do so. Take a look at [the documentation](https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.odeint.html) to know what the function is expecting as arguments. If the format of your _callable_ (the RHS function here) is not the one exptected, you could use a [lambda](https://realpython.com/python-lambda/) instead of copy/pasting or modifying your function.\n",
    "11. **Use a built-in function to solve the Cauchy problem**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy.integrate import odeint\n",
    "\n",
    "# Define a lambda to get NCL in the good format\n",
    "# Our RHS is independant of t, so we take it as\n",
    "# argument but it is actually unused.\n",
    "odefunc = lambda y, t: NCL(y)\n",
    "\n",
    "# Use odeint to solve the NCL problem and check that it converged to the solution (i.e. that the error is low)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "hide_input": false,
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.6"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
