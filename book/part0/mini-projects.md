# Mini projects

## Introduction

This mini-project aims to allow the students to practice their programming skills in
Python in a project of their choice. A practice session will be dedicated to these
projects but you can already think about it and even start to work.

## Description

The choice of the topic is free as long as the student hands a personal job which relies
on scientific programming. If possible, this mini-project can be coupled with projects
from other classes followed by the student in this semester.

```{admonition} Discuss about your topic!
Students are advised to discuss together and with one of the teachers about their
topic before starting. You can open a dedicated issue in
https://gricad-gitlab.univ-grenoble-alpes.fr/meige-legi/scientific-computing-m2-efm.
```

```{admonition} Workload
An indicative personal workload for the mini-project is about 6 hours in
addition of the dedicated practice session. This information should help the student to
choose a topic which will not require an unreasonable time investment.
```

```{admonition} Programming languages
We will study Python during this course so it will
be easier for you to use Python. However, you are free to use any open-source
tools. The tool has to be open-source because it has to be possible for the
teacher to try your code. Note that if you choose another language than Python, we
won't be able to help you about language related problems.
```

## Evaluation

The students have to put the code in a Gitlab project hosted on
<https://gricad-gitlab.univ-grenoble-alpes.fr>. To ease the evaluation, we ask you to
use a particular name: `miniproject-scientific-computing2022` (so that the full address
is
https://gricad-gitlab.univ-grenoble-alpes.fr/your-username/miniproject-scientific-computing2022).

You will have to post the address of the Gitlab project of your mini-project in a
message in the
[issue #27](https://gricad-gitlab.univ-grenoble-alpes.fr/meige-legi/scientific-computing-m2-efm/-/issues/27).

Then, the evaluation of these mini-projects will be done through a short oral sessions
(~ 10 minutes) where the students will be asked to present their work. It will
corresponds to half of the practical session grade.

## Examples of mini-projects

Please do not take a machine learning problem! There is a
[specific course in your Master 2](https://energy4climate.pages.in2p3.fr/public/education/machine_learning_for_climate_and_energy)
on this subject and there are many other things to do in scientific computing.

Remember that interesting and graphical representations are nice to show. Of course, if
you find something useful (for you or for others), it would be appreciated. You can also
look at examples of nice software projects to help you to choose a subject and a set of
technologies. Here is a small list of Python softare projects that can be interesting to
use: [matplotlib](https://matplotlib.org), [pandas](https://pandas.pydata.org/),
[ipyleaflet](https://ipyleaflet.readthedocs.io)/[voila](https://voila.readthedocs.io),
[pyscript](https://pyscript.net/),
[BeautifulSoup](https://beautiful-soup-4.readthedocs.io),
[textual](https://github.com/Textualize/textual)...

You can also focus on performance and try to use tools for high performance with Python
like PyPy (an alternative Python interpreter that can be installed with mamba) or
[Transonic](https://transonic.readthedocs.io)/Pythran/Numba. In this case, you should
present some benchmarks with nice figures.

```{important}
You can try to look for subjects and set of technologies by searching good
keywords on the web. If you want to work on a subject, try to find a good set
of software projects to work on it. Use your favorite web search engine!
```

```{important}
If you work on a program, it's much better if it can be installed by other
people. Read about [Python packaging](https://packaging.python.org)!
```

### Monte Carlo Integration

Read about the Monte Carlo integration numerical method (e.g.
https://en.wikipedia.org/wiki/Monte_Carlo_integration). In a similar way to what will be
done in the practice session, investigate and illustrate pros and cons of this method.
How does it compare to other methods that you know about ? In which situation can it be
useful ?

### Animation of simulations of particles in a box

Use Matplotlib to create an (interactive) animation of colliding particles in a box. An
extension to this project could be to link this animation to the ideal gas law.
