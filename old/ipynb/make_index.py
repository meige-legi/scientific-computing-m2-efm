from pathlib import Path
import sys

titles = {
    "Intro bash": "First steps with Linux and Bash",
    "Intro computing": "Introduction on scientific computing & why using Python for this course?",
    "Intro first steps": "First steps with Python",
    "Intro python language": "Some characteristics of the language",
    "Readwritefiles": "Read and write files",
    "Import standard library": "Import statements and the standart library",
    "Data struct": "Standard data structure (list, tuple, set and dict)",
    "Oop encapsulation": "Bonus: Object-oriented programming - encapsulation",
    "Oop inheritance": "Bonus: Object-oriented programming - inheritance",
    "Packaging": "Packaging, documentation and unittest",
    "Numpy scipy": "Numpy / Scipy",
    "Parallel": "Parallel computing (CPU bounded)",
    "Mercurial and gitlab": "Bonus: versioning with Mercurial and Gitlab",
    "Environnement": "Bonus: interpreters, distributions, packages",
    "Intro-open-source-sciences": "Introduction open-source in science",
}

this_dir = Path(__file__).parent

ipynb_files = sorted(tuple(this_dir.glob("*.ipynb")))

lines = []

for path in ipynb_files:
    name = path.with_suffix("").name
    index, title = name.split("_", 1)
    title = title.capitalize().replace("_", " ")

    if title in titles:
        title = titles[title]

    path_html = name + ".slides.html"
    lines.append(f"- `{title} <{path_html}>`_")

back = "\n"

code = """
Scientific computing course 2021
================================
"""

code += f"""
{back.join(lines)}

**Warning:** Save the address of this page in your browser! You will need to
come back to it many times.

"""

path_rst = this_dir / "index.rst"

if path_rst.exists():

    with open(path_rst) as file:
        old_code = file.read()

    if code == old_code:
        sys.exit(0)

print(f"override {path_rst}")

with open(path_rst, "w") as file:
    file.write(code)
