import sys

if sys.version_info[:2] < (3, 8):
    raise RuntimeError("Python version >= 3.8 required.")

try:
    import numpy, scipy, matplotlib, pandas
except ImportError:
    print(
        "Problem: numpy, scipy, matplotlib, pandas not importable\n"
        "Maybe run:\nconda install numpy scipy matplotlib pandas"
    )
else:
    print("Good! numpy, scipy, matplotlib, pandas importable!")
