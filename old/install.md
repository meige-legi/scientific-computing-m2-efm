# Setup your environment

We will have to use Python 3 (with Miniconda3), a good Python IDE (either
Spyder or VSCode), Jupyter and a version-control tool (Mercurial, or Git if you
know it and really like it).

In the following, we give indications about how to install these tools and how
to get the repository of this training locally on your computer. Please, if you
encounter problems, fill an issue
[here](https://gricad-gitlab.univ-grenoble-alpes.fr/meige-legi/scientific-computing-m2-efm/issues)
to explain what you did and what are the errors (please copy / paste the error
log).

Moreover, let's add that the best OS for HPC (and HPC with Python) is
Linux/GNU. Windows (at least without
[WSL](https://en.wikipedia.org/wiki/Windows_Subsystem_for_Linux)) and even
macOS are less adapted for this particular application. Python is a
cross-platform language but nevertheless, you will get a better experience for
HPC with Python on Linux.

#### Install miniconda and Python

The first step is to install miniconda3 (see
[here](https://docs.conda.io/en/latest/miniconda.html))

```bash
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
```

You have to answer "yes" to the last question about the initialization of
conda. When it's done, close the terminal (with `ctrl-d`) and reopen a new
terminal (with `ctrl-alt-t`). The word "(base)" should be in the line in the
terminal.

You need to activate the conda channel `conda-forge` with:

```bash
conda config --add channels conda-forge
```

and to install in your base conda environment the packages required for this course:

```bash
# download a requirement file
wget https://gricad-gitlab.univ-grenoble-alpes.fr/meige-legi/scientific-computing-m2-efm/-/raw/master/requirements.txt
# install with conda
conda install --file requirements.txt -y
```

You can check that your Python environment is all fine with:

```bash
python check_env.py
```

#### Install and setup Mercurial

To install and setup Mercurial, you can do:

```bash
pip install conda-app --no-cache-dir
conda-app install mercurial
```

Close the terminal (`ctrl-d`) and reopen a new terminal (`ctrl-alt-t`)! Then,
you should be able to create a default config file and edit it with the
command:

```bash
hg config --edit
```

You have to write something like:

```
[ui]
username=myusername <email@adress.org>
editor=nano
tweakdefaults = True

[extensions]
hgext.extdiff =
# only to use Mercurial with GitHub and Gitlab
hggit =

[extdiff]
cmd.meld =
```

By default, Mercurial will open the program `vi`. It's highly probable that you
won't like it! Just exit and save the file by writing `:x` and pressing the
enter key. Then reopen the file `~\.hgrc` with another program (for example
using gedit with the command `gedit ~\.hgrc`).

Warning: Mercurial **has to be correctly setup**! Since we will use [the Gitlab
instance of UGA](https://gricad-gitlab.univ-grenoble-alpes.fr), the Mercurial
extension hg-git has to be activated so the line `hggit =` in the configuration
file is mandatory.

#### Clone this repository

Once everything is installed and setup, you should be able to clone the
repository of the training with:

```
hg clone https://gricad-gitlab.univ-grenoble-alpes.fr/meige-legi/scientific-computing-m2-efm.git
```

Please [tell
us](https://gricad-gitlab.univ-grenoble-alpes.fr/meige-legi/scientific-computing-m2-efm/issues)
if it does not work.

Once the repository is cloned you should have a new directory
`scientific-computing-m2-efm` (launch the command `ls` to see it) and you
should be able to enter into it with `cd scientific-computing-m2-efm`.

#### Build the presentations

One needs Jupyter, rst2html5 (installable with `pip install rst2html5`), plus
`make` and few other Unix tools. Therefore, it is not easy to build the website
on Windows.

On Unix-like systems, `make presentations` should build the presentations and
`make serve` should start a server to visualize them in a browser.

#### Bonus

You can also install VSCode (see [here](https://code.visualstudio.com/download)).
